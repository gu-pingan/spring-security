package com.kfm.securityjwt.util;


/**
 * 通用常量信息
 */
public class Constants {
    /**
     * 令牌
     */
    public static final String TOKEN = "token";

    /**
     * 令牌前缀
     */
    public static final String LOGIN_USER_KEY = "login_user_key";
}
