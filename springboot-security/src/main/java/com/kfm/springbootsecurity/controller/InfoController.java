package com.kfm.springbootsecurity.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class InfoController {

    @GetMapping("/login")
    public ModelAndView login(){
        return new ModelAndView("login.html");
    }

    @GetMapping("/info")
    public String info(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        // 认证对象
        Object obj = authentication.getPrincipal();

        User user = (User) obj;
        return user.toString();
    }

    @GetMapping("/info2")
    public User info2(@AuthenticationPrincipal User user){
        return user;
    }

    @GetMapping("/index")
    public ModelAndView index(){
        return new ModelAndView("index.html");
    }
}
