<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- 在页面中引入 Security 标签 -->
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Security 标签</title>
</head>
<body>
    <!-- 使用 -->

    <!--
        authentication 代表的是当前认证对象
            property 获取 认证对象 的属性
            htmlEscape 表示是否需要将html进行转义。默认为true
            var 定义一个变量，存放 peoperty 获取的值，默认是存放在pageConext中
            scope 指定 var 存放的范围
     -->
    <security:authentication property="principal.username" var="user" scope="request" />

    <h1>${user }</h1>

    <hr />

    <!--
        authorize 判断权限
            access 权限判断的表达式, 这里如果 http 配置中禁用了表达式 （use-expressions="false"）
                    则这里需要启用。 可以将 http 配置中 use-expressions 改为 true
                    或者 自己定义一个 DefaultWebSecurityExpressionHandler bean。
                    这里我们使用第二种在 XML 中配置 bean
            url 表示如果用户拥有访问指定url的权限即表示可以显示authorize标签包含的内容
            method 表示用户应当具有指定url指定method访问的权限,method的默认值为GET，可选值为http请求的7种方法
                    一般配合 url 使用
            var 用于指定将权限鉴定的结果存放在pageContext的哪个属性中

     -->
<%--    <security:authorize access="" method="" url="" var=""></security:authorize>--%>

    <security:authorize access="hasRole('ADMIN')">
        当认证的用户有 ROLE_ADMIN 权限时显示此内容
    </security:authorize>
</body>
</html>
