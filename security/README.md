# 说明
当前项目是基于 xml 配置的 Spring-Security 的使用。

目录结构说明： 
```yml
  src/main  
    java - java源码
      com.kaifamiao
        controller 控制层
        model  实体
        service 业务层
    resources - 资源目录（配置文件）
      db.properties   - 数据库信息配置
      log4j.properties - log4j 日志信息配置
      spring.xml   - spring 配置文件
    webapp - web根目录
      WEB-INF/pages 页面
      web.xml

```

- Spring Security XML 配置在 `resources/spring.xml` 文件中
- Spring Security Tags 示例在 `webapp/WEB-INF/pages/hello.jsp`